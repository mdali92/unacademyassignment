package com.azhar.unacademyassignment

import android.graphics.Bitmap
import kotlinx.coroutines.*

class ServiceWorker(servicename: String) {

    //CREATES A DEDICATED THREAD FOR THIS SERVICEWORKER
    private val thread = newSingleThreadContext(servicename)

    fun addTask(task : Task<Bitmap>){
        CoroutineScope(thread).launch {
            val bitmap = async {
                task.onExecuteTask()
            }

            //THIS WILL ONLY BE EXECUTED ONCE THE ABOVE FETCHING TASK COMPLETES
            task.onTaskComplete(bitmap.await())
        }
    }

}