package com.azhar.unacademyassignment

interface Task<T> {
    fun onExecuteTask() : T
    fun onTaskComplete(t : T)
}