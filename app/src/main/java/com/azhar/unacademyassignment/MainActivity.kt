package com.azhar.unacademyassignment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.azhar.unacademyassignment.databinding.ActivityMainBinding
import okhttp3.OkHttpClient
import okhttp3.Request


class MainActivity() : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    val IMAGE1_URL = "https://i.picsum.photos/id/11/200/200.jpg"
    val IMAGE2_URL = "https://i.picsum.photos/id/188/200/200.jpg"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val serviceWorker1 = ServiceWorker("service_worker_1")
        val serviceWorker2 = ServiceWorker("service_worker_2")

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.button1.setOnClickListener {

            fetchImageAndSet(IMAGE1_URL, serviceWorker1, binding.image1)

        }

        binding.button2.setOnClickListener {

            fetchImageAndSet(IMAGE2_URL, serviceWorker2, binding.image2)

        }

    }

    private fun fetchImageAndSet(imageURL : String, serviceWorker : ServiceWorker, imageView: ImageView){
        val task = object : Task<Bitmap>{
            override fun onExecuteTask(): Bitmap {
                val okHttpClient = OkHttpClient()
                val request = Request.Builder().url(imageURL).build()
                val response = okHttpClient.newCall(request).execute()
                val bitmap = BitmapFactory.decodeStream(response.body?.byteStream())
                return bitmap
            }
            override fun onTaskComplete(bitmap: Bitmap) {
                runOnUiThread {
                    imageView.setImageBitmap(bitmap)
                }
            }
        }
        serviceWorker.addTask(task)
    }
}
